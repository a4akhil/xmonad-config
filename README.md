# xmonad-config
xmonad-config is my [xmonad](https://github.com/xmonad/xmonad) configuration was inspired by [DistroTube](https://gitlab.com/dwt1)

# Introduction 
If you are unfamiliar with xmonad, it is a tiling window manager that is notoriously minimal, stable, beautiful, and featureful. If you find yourself spending a lot of time organizing or managing windows, you may consider trying xmonad.

However, xmonad can be somewhat difficult to configure if you’re new to Haskell or even to xmonad itself.

This project contains a completely working and very usable xmonad configuration “out of the box”. If you are just starting out with xmonad, this will give you a configuration that I personally use every day. Thought it has been put into the colors, key bindings, layouts, and supplementary scripts to make life easier.

This project is also recommended for advanced xmonad users, who may just not want to reinvent the wheel. All source provided with this project is well documented and simple to customize.


## Screenshots

![Screenshot](/.xmonad/preview.png)

  
# Dependencies
xmonad

xmonad-contrib

[xmobar](https://github.com/jaor/xmobar)

[trayer](https://github.com/sargon/trayer-srg)

[dmenu](https://git.suckless.org/dmenu/)

[dunst](https://github.com/dunst-project/dunst)

[feh](https://github.com/derf/feh)

[picom](https://github.com/yshui/picom)

[pulse audio](https://github.com/pulseaudio/pulseaudio)

[i3-lock-fancy](https://github.com/meskarune/i3lock-fancy)

fontawesome



# Keyboard shortcuts
these are some basic key bindings, you can find more in my [xmonad.hs](https://gitlab.com/a4akhil/xmonad-config/-/blob/main/.xmonad/xmonad.hs)
| Key Binding | Actions|
| ------------- | ------------- |
| MODKEY + RETURN   | opens terminal (alacritty)  |
| MODKEY + SHIFT + RETURN  | opens run launcher (dmenu) |
|MODKEY + TAB| rotates through the available layouts|
| MODKEY + SPACE |toggles fullscreen on/off (useful for watching videos) |
| MODKEY + SHIFT + q | closes window with focus |
|MODKEY + 1-9| switch focus to workspace (1-9)|
| MODKEY + SHIFT + 1-9 | send focused window to workspace (1-9)  |
| MODKEY + h | shrink window (decreases window width)  |
|MODKEY + l| expand window (increases window width)|


## Installation



```bash

  git clone https://gitlab.com/a4akhil/xmonad-config.git .xmonad

```



# Personalizing or modifying xmonad-config

Once cloned, xmonad-config is laid out as follows.

All xmonad configuration is in ~/.xmonad/xmonad.hs. This includes things like key bindings, colors, layouts, etc. You may need to have some basic understanding of Haskell in order to modify this file, but most people have no problems.

Most of the xmobar configuration is in xmobar.hs.
